//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
//acceso seguro para MLAB
  var bodyParser = require('body-parser');
  app.use(bodyParser.json());
  app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Credentials", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
    next();
  });

  var requestjson = require('request-json');


var path = require('path');
var urlBaseMLab="https://api.mlab.com/api/1/databases/techu_practioner_bbva/collections/";
var apiKey = "?apiKey=uqwzZN70kJcdAbwOhpkewOBWe7JQeVPp";
var prefijo = "/api/1";
var cUser = "usuarios";
var cCtaMov ="cuentas_movimientos";


/*
/api/1/usuarios
CRUD
/api/1/movimientos
CRUD
/api/1/cuentas
CRUD
*/

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
//req - peticon
//res - respuesta

/*
/api/1/usuarios
CRUD
*/

/*
Insertar cliente
Con los siguientes campos, El idCliente no se puede repetir
Body
{
	"idCliente":4,
	"Nombre":"MONICA",
	"Apellido":"MORALES",
	"email":"monica.moralesbbva.com",
	"password":"1234567890"
}
*/
app.post(prefijo+"/usuarios",function(req,res){
  //res.send('Hola Mundo Node');
  var data = req.body;
  console.log(data);
  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.post(cUser+apiKey, data, function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});

//Login Consulta cliente y password para validar los datos del Login
//Datos de entrada idCliente y password
app.get(prefijo+"/usuarios/:idCliente&:password",function(req,res){

  var idCliente= req.params.idCliente;
  var pass= req.params.password;

  var clienteMLabP = requestjson.createClient(urlBaseMLab);
//  clienteMLabP.get(cUser+apiKey+'&q={"idCliente":'+idCliente+',"password"'+pass+'}', function(errCliente,resCliente,bodyCliente){
  clienteMLabP.get(cUser+apiKey+'&q={"$and": [{"idCliente": { "$eq": '+ idCliente +' } }, { "password": { "$eq": "'+pass+'" }}]}', function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});
//Consultar por idCliente
app.get(prefijo+"/usuarios/:idCliente",function(req,res){

  var idCliente= req.params.idCliente;

  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.get(cUser+apiKey+'&q={"idCliente":'+idCliente+'}', function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});

/*
  Actualizar cliente
  Dato de entrada idCliente
Con los siguientes campos, El idCliente no se puede repetir
Body
{
	"Nombre":"MONICA",
	"Apellido":"MORALES",
	"email":"monica.moralesbbva.com",
}
*/

app.put(prefijo+"/usuarios/:idCliente",function(req,res){
  //res.send('Hola Mundo Node');
  var idCliente= req.params.idCliente;
  var data = req.body;
  var dataCompleto =  { "$set":{} };
  dataCompleto["$set"]=data;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.put(cUser+apiKey+'&q={"idCliente":'+idCliente+'}', dataCompleto, function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});

/*
  Borrar cliente
  Datos de entrada idCliente

*/

app.delete(prefijo+"/usuarios/:idCliente", function(req,res){
//   res.send('Su peticion DELETE a sido resivida');

   var idCliente= req.params.idCliente;


   var clienteMLabP = requestjson.createClient(urlBaseMLab);
   clienteMLabP.get(cUser+apiKey+'&q={"idCliente":'+idCliente+'}'+'&fo=true', function(errClienteGet,resClienteGet,bodyClienteGet){

     if(errClienteGet){
        console.log(errClienteGet);
      } else {
        console.log(bodyClienteGet);
        var idMongo = bodyClienteGet._id['$oid'];

        clienteMLabP.del(cUser+"/"+idMongo+apiKey, function(errCliente,resCliente,bodyCliente){
          if(errCliente){
             console.log(errCliente);
           } else {
             res.send({"resultado":"Registro borrado"});
           }
        });

      }

   });
});



/*
/api/1/cuentas
CRUD
*/
// Agregar una cuenta
app.post(prefijo+"/cuentas_movimientos",function(req,res){

  var data = req.body;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.post(cCtaMov+apiKey, data, function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});

/*
/api/1/movimientos
CRUD
*/
/*
Inserta movimientos en la Cuenta
Datos de entrada idCuenta, no se puede repetir id del movimiento

Body
{
      "id": 1,
      "fecha": {"$date": "2018-11-136T23:23:50Z"},
      "importe": 16.30,
      "tipo":"tipo de movimiento"     --Los Tipos son: pago_nomina, pago_tarjeta, retiro_efectivo, compra
    }

*/

//Movimientos
//Put - insertar
app.put(prefijo+"/cuentas_movimientos/:Cliente",function(req,res){
  //res.send('Hola Mundo Node');
  var Cliente= req.params.Cliente;
  var data = req.body;
  var dataCompleto =  { "$addToSet":{"movimientos":{}} };
  dataCompleto["$addToSet"]["movimientos"]=data;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);
  clienteMLabP.put(cCtaMov+apiKey+'&q={"Cliente":'+Cliente+'}', dataCompleto, function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});


//Consulta de movimientos de una cuenta
//Datos de entrada idCuenta
/**
Regresa los datos de la siguiente forma:
[
    {
        "_id": {
            "$oid": "5bd7b91f5d0e65692ee8bf8c"
        },
        "idCuenta": 1,
        "Cliente": 1,
        "movimientos": [
            {
                "id": 1,
                "fecha": {
                    "$date": "2018-10-28T23:23:50.000Z"
                },
                "importe": 15.2,
                "tipo": "retiro_efectivo"
            },
            {
                "id": 2,
                "fecha": {
                    "$date": "2018-10-26T23:23:50.000Z"
                },
                "importe": 15.2,
                "tipo": "pago_tarjeta"
            }
        ]
    }
]

*/

app.get(prefijo+"/cuentas_movimientos/:Cliente",function(req,res){

  var cliente= req.params.Cliente;
  var clienteMLabP = requestjson.createClient(urlBaseMLab);

  clienteMLabP.get(cCtaMov+apiKey+'&q={"Cliente":'+cliente+'}', function(errCliente,resCliente,bodyCliente){

    if(errCliente){
       console.log(errCliente);
     } else {
       res.send(bodyCliente);
     }

  });

});
